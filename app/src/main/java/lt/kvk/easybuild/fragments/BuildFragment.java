package lt.kvk.easybuild.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import lt.kvk.easybuild.MainActivity;
import lt.kvk.easybuild.R;
import lt.kvk.easybuild.model.Graphics;
import lt.kvk.easybuild.model.Motherboard;
import lt.kvk.easybuild.model.PSU;
import lt.kvk.easybuild.model.Processor;
import lt.kvk.easybuild.model.RAM;
import lt.kvk.easybuild.model.Storage;
import lt.kvk.easybuild.sqlite.MySQLiteHelper;

public class BuildFragment extends Fragment {

    @Bind(R.id.spinner_motherboard) Spinner spinner_motherboard;
    @Bind(R.id.spinner_processor) Spinner spinner_processor;
    @Bind(R.id.spinner_gpu) Spinner spinner_gpu;
    @Bind(R.id.spinner_ram) Spinner spinner_ram;
    @Bind(R.id.spinner_storage) Spinner spinner_storage;
    @Bind(R.id.spinner_psu) Spinner spinner_psu;
    @Bind(R.id.kaina) TextView tfKaina;

    List<Processor> processors;
    List<Motherboard> motherboards;
    List<Graphics> graphicsList;
    List<Storage> storages;
    List<RAM> rams;
    List<PSU> psus;

    int id;
    int kaina;
    MySQLiteHelper helper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_build,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        spinner_processor.setEnabled(false);
        spinner_gpu.setEnabled(false);
        spinner_ram.setEnabled(false);
        spinner_psu.setEnabled(false);
        spinner_storage.setEnabled(false);

        //get all the computer parts
        helper = MySQLiteHelper.getInstance(getActivity());
        doSomeWork();
        motherboards = helper.getMotherboards("" + id);
        graphicsList = helper.getGraphics("" + id);
        storages = helper.getStorages();

        List<String> motinines = new ArrayList<String>();
        for (Motherboard motherboard : motherboards) {
            motinines.add(motherboard.getPrice()+"€ | "+motherboard.getName());
        }
        setupSelection("Motininė plokštė",motinines,spinner_motherboard);
        setupSelection("Procesorius", new ArrayList<String>(), spinner_processor);

        List<String> grafika = new ArrayList<String>();
        for(Graphics gpu: graphicsList){
            grafika.add(gpu.getPrice()+"€ | "+gpu.getName());
        }
        setupSelection("Grafinė plokštė", grafika, spinner_gpu);
        setupSelection("Atmintis",new ArrayList<String>(),spinner_ram);
        List<String> storage = new ArrayList<String>();
        for(Storage stor: storages){
            storage.add(+stor.getPrice()+"€ | "+stor.getType()+" | "+stor.getSize()+" | "+stor.getBrand());
        }
        setupSelection("Laikmena",storage,spinner_storage);
        setupSelection("Maitinimo blokas", new ArrayList<String>(), spinner_psu);
    }

    public void doSomeWork(){
        id = getArguments().getInt("class");
        String title = "EasyBuild";

        if(id==1){
            title="Naršymui";
        }else if(id==2){
            title="Biuro programoms";
        }else if(id==3){
            title="Grafikai";
        }else{
            title="Žaidimams";
        }
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(title);
    }

    public void setupSelection(String defaultValue, List<String> items, Spinner spinner){
        items.add(0,defaultValue);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1,items);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);
    }

    @OnClick(R.id.btn_motherboard_info)
    public void onClickMotherboard(){
        if(spinner_motherboard.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_motherboard.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        Motherboard moth = motherboards.get(spinner_motherboard.getSelectedItemPosition() - 1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(moth!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+moth.getPrice()+"€<br/><b>Pavadinimas: </b>"+moth.getName()+"<br/><b>Lizdas: </b>"+moth.getSocket()+"<br/><b>Lustas: </b>"+
            moth.getChip()+"<br/><b>Operatyviosios atminties tipas: </b>"+moth.getRam()+"<br/><b>Integruotos grafinės plokštės palaikymas: </b>"+moth.getIgps()+"<br/><b>Tipas: </b>"+moth.getType()+"<br/><b></b>"));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnClick(R.id.btn_processor_info)
    public void onClick(){
        if(spinner_processor.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_processor.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        Processor cpu = processors.get(spinner_processor.getSelectedItemPosition()-1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(cpu!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+cpu.getPrice()+"€<br/><b>Pavadinimas: </b>"+cpu.getName()+"<br/><b>Generacija: </b>"+cpu.getCodename()+"<br/>" +
                    "<b>Branduoliai: </b>"+cpu.getCores()+"<br/><b>Lizdas: </b>"+cpu.getSocket()+"<br/><b>Papildoma informacija: </b>"+cpu.getProcess()+"<br/><b>Dažnis: </b>"+cpu.getClock()+"<br/>" +
                    "<b>Cache: </b>"+cpu.getCache()+"<br/><b>Galia: </b>"+cpu.getTdp()+"<br/><b>Išleidimo data: </b>"+cpu.getRelease_date()+"<br/><b>PassMark įvertinimas: </b>"+cpu.getBenchmark()+"<br/>"));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnClick(R.id.btn_graphics_info)
    public void onClickGraphics(){
        if(spinner_gpu.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_gpu.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        Graphics graph = graphicsList.get(spinner_gpu.getSelectedItemPosition() - 1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(graph!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+graph.getPrice()+"€<br/><b>Pavadinimas: </b>"+graph.getName()+"<br/><b>Lustas: </b>"+graph.getChip()+"<br/><b>Jungtis: </b>"+
            graph.getBus()+"<br/><b>Atmintis: </b>"+graph.getMemory()+"<br/><b>GPU Dažnis: </b>"+graph.getGpu_clock()+"<br/><b>Atminties dažnis: </b>"+graph.getMem_clock()+"<br/><b>Shaders/TMUs/ROPs: </b>"+graph.getShader()+"<br/><b>Išleidimo data: </b>"+graph.getRelease()+"<br/><b>Rekomenduojamas maitinimo blokas: +</b>"+
                    graph.getWatts()+"<br/><b>PassMark įvertinimas: </b>"+graph.getBenchmark()));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnClick(R.id.btn_psu_info)
    public void onClickPSU(){
        if(spinner_psu.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_psu.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        PSU item = psus.get(spinner_psu.getSelectedItemPosition() - 1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(item!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+item.getPrice()+"€<br/><b>Pavadinimas: </b>"+item.getModel()+"<br/><b>Galia: </b>"+
                    item.getWatt()+"<br/><b>Tipas: </b>"+item.getType()+"<br/><b>Saugumo technologijos: </b>"+item.getSafty()));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnClick(R.id.btn_ram_info)
    public void onClickRAM(){
        if(spinner_ram.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_ram.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        RAM item = rams.get(spinner_ram.getSelectedItemPosition() - 1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(item!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+item.getPrice()+"€<br/><b>Pavadinimas: </b>"+item.getBrand()+"<br/><b>Tipas: </b>"+
                    item.getType()+"<br/><b>Dažnis: </b>"+item.getClock()+"<br/><b>Dydis: </b>"+item.getSize()+"<br/><b>Radiatorius: </b>"+item.getCooler()));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnClick(R.id.btn_storage_info)
    public void onClickStorage(){
        if(spinner_storage.getSelectedItemPosition()==0){
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(spinner_storage.getSelectedItem().toString());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_layout, null);

        Storage item = storages.get(spinner_storage.getSelectedItemPosition() - 1);
        TextView contentText = (TextView) dialogLayout.findViewById(R.id.tf_dialog);

        if(item!=null) {
            contentText.setText(Html.fromHtml("<b>Kaina: </b>"+item.getPrice()+"€<br/><b>Pavadinimas: </b>"+item.getBrand()+"<br/><b>Dydis: </b>"+item.getSize()+"<br/><b>Buferis: </b>"+item.getBuffer()
                    +"<br/><b>Jungtis: </b>"+item.getConnection()+"<br/><b>Tipas: </b>"+item.getType()));
        }
        builder.setView(dialogLayout);
        builder.show();
    }

    @OnItemSelected(R.id.spinner_motherboard)
    public void onItemSelected(int position){
        if(position==0){
            return;
        }
        Motherboard motherboard = motherboards.get(position-1);
        processors = helper.getProcessors(id, motherboard.getSocket());
        rams = helper.getRAMs(motherboards.get(position - 1).getRam());

        if(processors!=null){
            Log.d("debug","processors "+processors.size());
            List<String> procesoriai = new ArrayList<String>();
            for(Processor processor: processors){
                procesoriai.add(processor.getPrice()+"€ | "+processor.getName());
            }
            setupSelection("Procesorius", procesoriai, spinner_processor);
            spinner_processor.setEnabled(true);
        }
        if(rams!=null){
            List<String> ramai = new ArrayList<String>();
            for(RAM ram: rams){
                ramai.add(+ram.getPrice()+"€ | "+ram.getBrand()+ " | "+ram.getSize()+"MB");
            }
            setupSelection("Atmintis", ramai, spinner_ram);
            spinner_ram.setEnabled(true);
        }

        updatePrice();
        tfKaina.setText(kaina + " Euru");
    }
    @OnItemSelected(R.id.spinner_processor)
    public void onProcessorSelected(int position){
        if(position==0){
            return;
        }
        spinner_gpu.setEnabled(true);
        updatePrice();
        tfKaina.setText(kaina + " Euru");
    }
    @OnItemSelected(R.id.spinner_gpu)
    public void onGraphicsSelected(int position){
        if(position==0 || spinner_processor.getSelectedItemPosition()==0){
            return;
        }// cpu tdp + gpu vat. = psu watt (cpu nuimti W raide)
        //String cpu = processors.get(spinner_processor.getSelectedItemPosition()-1).getTdp();
        //cpu = cpu.replace("W","");
        //String gpu = graphicsList.get(position-1).getWatts();
        //int suma = Integer.parseInt(cpu) + Integer.parseInt(gpu);

        psus = helper.getPSUs();
        if(psus!=null){
            List<String> psusai = new ArrayList<String>();
            for(PSU psu: psus){
                psusai.add(psu.getPrice()+"€ | "+ psu.getModel()+" | "+psu.getWatt()+"W");
            }
            setupSelection("Maitinimo blokas", psusai, spinner_psu);
        }

        spinner_ram.setEnabled(true);
        updatePrice();
        tfKaina.setText(kaina + " Euru");
    }
    @OnItemSelected(R.id.spinner_ram)
    public void onRAMSelected(int position){
        if(position==0){
            return;
        }
        spinner_storage.setEnabled(true);
        updatePrice();
        tfKaina.setText(kaina + " Euru");
    }
    @OnItemSelected(R.id.spinner_storage)
    public void onStorageSelected(int position){
        if(position==0){
            return;
        }
        spinner_psu.setEnabled(true);
        updatePrice();
        tfKaina.setText(kaina + " Euru");
    }
    @OnItemSelected(R.id.spinner_psu)
    public void onPSUSelected(int position){
        if(position==0){
            return;
        }
        updatePrice();
        tfKaina.setText(kaina+" Euru");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void updatePrice(){
        int kainaM=0;
        int kainaG=0;
        int kainaP=0;
        int kainaPSU=0;
        int kainaR=0;
        int kainaS=0;

        try {
            kainaM = motherboards.get(spinner_motherboard.getSelectedItemPosition() - 1).getPrice();
            kainaP = processors.get(spinner_processor.getSelectedItemPosition() - 1).getPrice();
            kainaG = graphicsList.get(spinner_gpu.getSelectedItemPosition() - 1).getPrice();
            kainaPSU = psus.get(spinner_psu.getSelectedItemPosition() - 1).getPrice();
            kainaR = rams.get(spinner_ram.getSelectedItemPosition() - 1).getPrice();
            kainaS = storages.get(spinner_storage.getSelectedItemPosition() - 1).getPrice();
        }catch(Exception ex){}

        kaina = kainaM+kainaP+kainaG+kainaPSU+kainaR+kainaS;
    }

    class MySpinnerAdapter extends BaseAdapter {

        Context c;
        ArrayList<String> objects;

        public MySpinnerAdapter(Context context, ArrayList<String> objects){
            super();
            this.c=context;
            this.objects = objects;
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public Object getItem(int position) {
            return objects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return null;
        }
    }
}
