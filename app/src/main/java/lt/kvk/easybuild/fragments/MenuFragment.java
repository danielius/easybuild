package lt.kvk.easybuild.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lt.kvk.easybuild.MainActivity;
import lt.kvk.easybuild.R;
import lt.kvk.easybuild.model.MenuItem;

public class MenuFragment extends Fragment {

    @Bind(R.id.recycler)
    RecyclerView recycler;
    private MyRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        adapter = new MyRecyclerAdapter(prepareData());
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("EasyBuild");
    }

    // menu pasirinkimai
    private List<MenuItem> prepareData() {
        List<MenuItem> items = new ArrayList<MenuItem>();
        MenuItem item = null;

        item = new MenuItem();
        item.setTitle("Žaidimams");
        item.setSubtitle("Kaina nuo 800€");
        item.setIconName(R.mipmap.menu_game);
        items.add(item);

        item = new MenuItem();
        item.setTitle("Kompiuterinei grafikai");
        item.setSubtitle("Kaina nuo 500€");
        item.setIconName(R.mipmap.menu_pallete);
        items.add(item);

        item = new MenuItem();
        item.setTitle("Biuro programoms");
        item.setSubtitle("Kaina nuo 250€");
        item.setIconName(R.mipmap.menu_business);
        items.add(item);

        item = new MenuItem();
        item.setTitle("Naršymui internete");
        item.setSubtitle("Kaina nuo 200€");
        item.setIconName(R.mipmap.menu_internet);
        items.add(item);

        return items;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private class MyRecyclerAdapter extends RecyclerView.Adapter<ViewHolder>{

        List<MenuItem> items;

        public MyRecyclerAdapter(List<MenuItem> items){
            this.items=items;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getActivity().getLayoutInflater().inflate(R.layout.row_items,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            //set values here
            holder.title.setText(items.get(position).getTitle());
            holder.subtitle.setText(items.get(position).getSubtitle());
            holder.icon.setImageResource(items.get(position).getIconName());
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.item_title)
        TextView title;
        @Bind(R.id.item_text)
        TextView subtitle;
        @Bind(R.id.item_icon)
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.card_view)
        void onClick(){
            int id = getAdapterPosition();

            if(id==0){
                id=4;
            }else if(id==1){
                id=3;
            }else if(id==2){
                id=2;
            }else{
                id=1;
            }

            Bundle basket = new Bundle();
            basket.putInt("class", id);
            BuildFragment fragment = new BuildFragment();
            fragment.setArguments(basket);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container,fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }
}
