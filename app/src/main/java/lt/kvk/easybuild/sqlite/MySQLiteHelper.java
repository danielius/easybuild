package lt.kvk.easybuild.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lt.kvk.easybuild.model.Graphics;
import lt.kvk.easybuild.model.Motherboard;
import lt.kvk.easybuild.model.PSU;
import lt.kvk.easybuild.model.Processor;
import lt.kvk.easybuild.R;
import lt.kvk.easybuild.model.RAM;
import lt.kvk.easybuild.model.Storage;

/**
 * Created by Danielius on 07-Dec-15.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Duomenys";
    private static final int DATABASE_VERSION = 10;

    private static MySQLiteHelper helper;
    Context context;

    public static MySQLiteHelper getInstance(Context context){
        if(helper==null){
            helper = new MySQLiteHelper(context.getApplicationContext());
        }
        return helper;
    }

    public MySQLiteHelper(Context applicationContext) {
        super(applicationContext,DATABASE_NAME,null, DATABASE_VERSION);
        this.context = applicationContext;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE cpu (" +
                "  price varchar(10), name varchar(50), codename varchar(50), cores varchar(50)," +
                "  socket varchar(20), process varchar(20), clock varchar(20), cache varchar(50)," +
                "  tdp varchar(50), release varchar(50), benchmark varchar(50), class varchar(10))";
        db.execSQL(query);

        query ="CREATE TABLE gpu (" +
                "  price varchar(10),name varchar(50),chip varchar(50)," +
                "  bus varchar(50),memory varchar(200),gpu_clock varchar(50)," +
                "  mem_clock varchar(50),shader varchar(100),release varchar(100)," +
                "  watts varchar(100), benchmark varchar(100), class varchar(50))";
        db.execSQL(query);

        query ="CREATE TABLE motherboard (" +
                "  price varchar(50), name varchar(50), socket varchar(50)," +
                "  chip varchar(100), ram varchar(50), igps varchar(100)," +
                "  type varchar(50), class varchar(50) DEFAULT '0')";
        db.execSQL(query);

        query ="CREATE TABLE psu (" +
                "  price varchar(50),model varchar(500),watt varchar(100)," +
                "  type varchar(100),connectors varchar(500)," +
                "  safty varchar(500))";
        db.execSQL(query);

        query ="CREATE TABLE ram (" +
                "  price varchar(50),brand varchar(100),type varchar(100)," +
                "  clock varchar(100),size varchar(50),cooler varchar(50)," +
                "  class varchar(50))";
        db.execSQL(query);

        query ="CREATE TABLE storage (" +
                "  price varchar(50),brand varchar(200),size varchar(100)," +
                "  buffer varchar(50), connection varchar(50), type varchar(50))";
        db.execSQL(query);

        readAndSaveData(db);
        //TODO method for values from txt
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS cpu");
        db.execSQL("DROP TABLE IF EXISTS gpu");
        db.execSQL("DROP TABLE IF EXISTS motherboard");
        db.execSQL("DROP TABLE IF EXISTS psu");
        db.execSQL("DROP TABLE IF EXISTS ram");
        db.execSQL("DROP TABLE IF EXISTS storage");

        this.onCreate(db);
    }

    public void addItem(SQLiteDatabase db){
        ContentValues values = new ContentValues();

        values.put("price", "120");
        values.put("name","Intel i3-2100");
        values.put("codename", "codename");
        values.put("cores","2");
        values.put("socket", "LGA1155");
        values.put("process", "process");
        values.put("clock", "3.10Ghz");
        values.put("cache", "Taip");
        values.put("tdp", "65w");
        values.put("release", "release date");
        values.put("class", "1");
        db.insert("cpu", null, values);
    }

    public List<Processor> getProcessors(int klase, String socket){
        List<Processor> processors = new ArrayList<Processor>();
        String query = "SELECT * FROM cpu WHERE class = ? AND socket = ?";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{""+klase,""+socket});

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                Processor processor = new Processor();
                processor.setPrice(Integer.parseInt(cursor.getString(0)));
                processor.setName(cursor.getString(1));
                processor.setCodename(cursor.getString(2));
                processor.setCores(cursor.getString(3));
                processor.setSocket(cursor.getString(4));
                processor.setProcess(cursor.getString(5));
                processor.setClock(cursor.getString(6));
                processor.setCache(cursor.getString(7));
                processor.setTdp(cursor.getString(8));
                processor.setRelease_date(cursor.getString(9));
                processor.setBenchmark(cursor.getString(10));
                processor.setKlase(cursor.getString(11));
                processors.add(processor);
            }while(cursor.moveToNext());
        }
        db.close();
        return processors;
    }

    public List<Motherboard> getMotherboards(String klase){
        List<Motherboard> motherboards = new ArrayList<Motherboard>();
        String query = "SELECT * FROM motherboard where class = ? OR class = ?";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{"0",""+klase});

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                Motherboard motherboard = new Motherboard();
                motherboard.setPrice(Integer.parseInt(cursor.getString(0)));
                motherboard.setName(cursor.getString(1));
                motherboard.setSocket(cursor.getString(2));
                motherboard.setChip(cursor.getString(3));
                motherboard.setRam(cursor.getString(4));
                motherboard.setIgps(cursor.getString(5));
                motherboard.setType(cursor.getString(6));
                motherboard.setKlase(cursor.getString(7));
                motherboards.add(motherboard);
            }while(cursor.moveToNext());
        }
        db.close();
        return motherboards;
    }

    public List<Graphics> getGraphics(String klase){
        List<Graphics> graphicsList = new ArrayList<Graphics>();
        String query = "SELECT * FROM gpu where class = ?";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{""+klase});

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                Graphics graphics = new Graphics();
                graphics.setPrice(Integer.parseInt(cursor.getString(0)));
                graphics.setName(cursor.getString(1));
                graphics.setChip(cursor.getString(2));
                graphics.setBus(cursor.getString(3));
                graphics.setMemory(cursor.getString(4));
                graphics.setGpu_clock(cursor.getString(5));
                graphics.setMem_clock(cursor.getString(6));
                graphics.setShader(cursor.getString(7));
                graphics.setRelease(cursor.getString(8));
                graphics.setWatts(cursor.getString(9));
                graphics.setBenchmark(cursor.getString(10));
                graphics.setKlase(cursor.getString(11));
                graphicsList.add(graphics);
            }while(cursor.moveToNext());
        }
        db.close();
        return graphicsList;
    }

    public List<PSU> getPSUs(){
        List<PSU> PSUs = new ArrayList<PSU>();
        String query = "SELECT * FROM psu";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                PSU psu = new PSU();
                psu.setPrice(Integer.parseInt(cursor.getString(0)));
                psu.setModel(cursor.getString(1));
                psu.setWatt(cursor.getString(2));
                psu.setType(cursor.getString(3));
                psu.setConnectors(cursor.getString(4));
                psu.setSafty(cursor.getString(5));
                PSUs.add(psu);
            }while(cursor.moveToNext());
        }
        db.close();
        return PSUs;
    }

    public List<RAM> getRAMs(String type){
        List<RAM> rams = new ArrayList<RAM>();
        String query = "SELECT * FROM ram where class = ? AND type = ?";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{"0",""+type});

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                RAM ram = new RAM();
                String price = cursor.getString(0);
                price = price.replace(",",".");
                ram.setPrice(Integer.parseInt(price));
                ram.setBrand(cursor.getString(1));
                ram.setType(cursor.getString(2));
                ram.setClock(cursor.getString(3));
                ram.setSize(cursor.getString(4));
                ram.setCooler(cursor.getString(5));
                ram.setKlase(cursor.getString(6));
                rams.add(ram);
            }while(cursor.moveToNext());
        }
        db.close();
        return rams;
    }

    public List<Storage> getStorages(){
        List<Storage> storages = new ArrayList<Storage>();
        String query = "SELECT * FROM storage";

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.getCount( )==0){
            return null;
        }

        if(cursor.moveToFirst()){
            do{
                Storage storage = new Storage();
                storage.setPrice(Integer.parseInt(cursor.getString(0)));
                storage.setBrand(cursor.getString(1));
                storage.setSize(cursor.getString(2));
                storage.setBuffer(cursor.getString(3));
                storage.setConnection(cursor.getString(4));
                storage.setType(cursor.getString(5));
                storages.add(storage);
            }while(cursor.moveToNext());
        }
        db.close();
        return storages;
    }

    public void readAndSaveData(SQLiteDatabase db){
        Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.cpu_amd_sql_eur));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.cpu_intel_sql_eur));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.gpu_amd_sql));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.gpu_nvidia_sql));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();
        scanner = new Scanner(context.getResources().openRawResource(R.raw.motherboard));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.ram));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.psu));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();

        scanner = new Scanner(context.getResources().openRawResource(R.raw.storage));
        while(scanner.hasNextLine()){
            String data = scanner.nextLine() +" "+ scanner.nextLine();
            db.execSQL(data);
        }
        scanner.close();
    }
}
