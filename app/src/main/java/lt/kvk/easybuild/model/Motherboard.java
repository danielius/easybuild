package lt.kvk.easybuild.model;

/**
 * Created by Danielius on 14-Dec-15.
 */
public class Motherboard {

    private int price;
    private String name;
    private String socket;
    private String chip;
    private String ram;
    private String igps;
    private String type;
    private String klase;

    public String getKlase() {
        return klase;
    }

    public void setKlase(String klase) {
        this.klase = klase;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String brand) {
        this.name = brand;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getIgps() {
        return igps;
    }

    public void setIgps(String igps) {
        this.igps = igps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
