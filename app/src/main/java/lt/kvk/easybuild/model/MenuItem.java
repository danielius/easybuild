package lt.kvk.easybuild.model;

/**
 * Created by Danielius on 16-Nov-15.
 */
public class MenuItem {
    private String title;
    private String subtitle;
    private int iconName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconName() {
        return iconName;
    }

    public void setIconName(int iconName) {
        this.iconName = iconName;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
