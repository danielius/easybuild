package lt.kvk.easybuild.model;

/**
 * Created by Danielius on 28-Dec-15.
 */
public class PSU {

    private int price;
    private String model;
    private String watt;
    private String type;
    private String connectors;
    private String safty;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getWatt() {
        return watt;
    }

    public void setWatt(String watt) {
        this.watt = watt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getConnectors() {
        return connectors;
    }

    public void setConnectors(String connectors) {
        this.connectors = connectors;
    }

    public String getSafty() {
        return safty;
    }

    public void setSafty(String safty) {
        this.safty = safty;
    }
}
