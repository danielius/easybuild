package lt.kvk.easybuild.model;

/**
 * Created by Danielius on 15-Dec-15.
 */
public class Graphics {

    private int price;
    private String name;
    private String chip;
    private String bus;
    private String memory;
    private String gpu_clock;
    private String mem_clock;
    private String shader;
    private String release;
    private String watts;
    private String benchmark;
    private String klase;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getGpu_clock() {
        return gpu_clock;
    }

    public void setGpu_clock(String gpu_clock) {
        this.gpu_clock = gpu_clock;
    }

    public String getMem_clock() {
        return mem_clock;
    }

    public void setMem_clock(String mem_clock) {
        this.mem_clock = mem_clock;
    }

    public String getShader() {
        return shader;
    }

    public void setShader(String shader) {
        this.shader = shader;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getWatts() {
        return watts;
    }

    public void setWatts(String watts) {
        this.watts = watts;
    }

    public String getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(String benchmark) {
        this.benchmark = benchmark;
    }

    public String getKlase() {
        return klase;
    }

    public void setKlase(String klase) {
        this.klase = klase;
    }
}
