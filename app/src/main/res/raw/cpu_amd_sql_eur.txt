INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('22', 'AMD Sempron 130', 'Sargas', '1-Core', 'AMD Socket AM3', '45 nm', '2600 MHz', '128K / 512K / 0K', '45W', 'Rugpjutis 2011', '673', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('38', 'AMD E2-1800', 'Zacate', '2-Core', 'AMD Socket FT1', '40 nm', '1700 MHz', '64K / 512K / 0K', '18W', 'Birzelis 2012', '829', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('70', 'AMD E2-3200', 'Llano', '2-Core', 'AMD Socket FM1', '32 nm', '2400 MHz', '128K / 512K / 0K', '65W', 'Rugsejis 2011', '1486', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('33', 'AMD A4-3300', 'Llano', '2-Core', 'AMD Socket FM1', '32 nm', '2500 MHz', '128K / 512K / 0K', '65W', 'Rugsejis 2011', '1536', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('100', 'AMD A4-3400', 'Llano', '2-Core', 'AMD Socket FM1', '32 nm', '2700 MHz', '128K / 512K / 0K', '65W', 'Rugsejis 2011', '1595', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('36', 'AMD A4-3420', 'Llano', '2-Core', 'AMD Socket FM1', '32 nm', '2800 MHz', '128K / 512K / 0K', '65W', 'Gruodis 2011', '1681', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('47', 'AMD A4-4000', 'Richland', '2-Core', 'AMD Socket FM2', '32 nm', '3000 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2013', '1804', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('34', 'AMD A4-5300B', 'Trinity', '2-Core', 'AMD Socket FM2', '32 nm', '3400 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '1934', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('70', 'AMD Athlon II X2 270', 'Regor', '2-Core', 'AMD Socket AM3', '45 nm', '3400 MHz', '128K / 1024K / 0K', '65W', 'Liepa 2011', '1998', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('60', 'AMD A4-5300', 'Trinity', '2-Core', 'AMD Socket FM2', '32 nm', '3400 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '2005', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('127', 'AMD A6-3500', 'Llano', '3-Core', 'AMD Socket FM1', '32 nm', '2100 MHz', '128K / 1024K / 0K', '65W', 'Rugpjutis 2011', '2019', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('40', 'AMD A6-5400K', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3600 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '2137', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('54', 'AMD A6-5400B', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3600 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '2142', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('136', 'AMD Athlon II X2 280', 'Regor', '2-Core', 'AMD Socket AM3', '45 nm', '3600 MHz', '128K / 1024K / 0K', '65W', 'Vasaris 2013', '2191', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('54', 'AMD A4-6300', 'Richland', '2-Core', 'AMD Socket FM2', '32 nm', '3700 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2013', '2237', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('88', 'AMD Athlon II X3 425e', 'Rana', '3-Core', 'AMD Socket AM3', '45 nm', '2700 MHz', '128K / 512K / 0K', '45W', 'Geguze 2011', '2239', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('58', 'AMD A6-6400K', 'Richland', '2-Core', 'AMD Socket FM2', '32 nm', '3900 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2013', '2275', '1');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('63', 'AMD A6-3600', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2100 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2011', '2762', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('54', 'AMD A6-3620', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2200 MHz', '128K / 1024K / 0K', '65W', 'Gruodis 2011', '2857', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('92', 'AMD Athlon II X4 620e', 'Propus', '4-Core', 'AMD Socket AM3', '45 nm', '2600 MHz', '128K / 512K / 0K', '45W', 'Geguze 2011', '2906', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('145', 'AMD Athlon II X3 460', 'Rana', '3-Core', 'AMD Socket AM3', '45 nm', '3400 MHz', '128K / 512K / 0K', '95W', 'Geguze 2011', '2914', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('82', 'AMD A8-3800', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2400 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2011', '3148', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('161', 'AMD Athlon II X4 631', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2600 MHz', '128K / 1024K / 0K', '100W', 'Rugpjutis 2011', '3178', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('87', 'AMD A6-3650', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2600 MHz', '128K / 1024K / 0K', '100W', 'Birzelis 2011', '3202', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('72', 'AMD A8-3820', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2500 MHz', '128K / 1024K / 0K', '65W', 'Gruodis 2011', '3217', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('59', 'AMD Athlon II X4 638', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2700 MHz', '128K / 1024K / 0K', '65W', 'Vasaris 2012', '3257', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('64', 'AMD Athlon II X4 641', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2800 MHz', '128K / 1024K / 0K', '100W', 'Vasaris 2012', '3437', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('95', 'AMD A8-3850', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '2900 MHz', '128K / 1024K / 0K', '100W', 'Birzelis 2011', '3521', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('172', 'AMD Phenom II X4 840', 'Propus', '4-Core', 'AMD Socket AM3', '45 nm', '3200 MHz', '128K / 512K / 0K', '95W', 'Sausis 2011', '3566', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('100', 'AMD Phenom II X4 850', 'Propus', '4-Core', 'AMD Socket AM3', '45 nm', '3300 MHz', '128K / 512K / 0K', '95W', 'Birzelis 2011', '3567', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('162', 'AMD Athlon II X4 651', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '3000 MHz', '128K / 1024K / 0K', '100W', 'Lapkritis 2011', '3611', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('118', 'AMD A8-3870K', 'Llano', '4-Core', 'AMD Socket FM1', '32 nm', '3000 MHz', '128K / 1024K / 0K', '100W', 'Gruodis 2011', '3622', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('82', 'AMD A8-5500B', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3200 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '3733', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('249', 'AMD A10-6700T', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '2500 MHz', '128K / 1024K / 0K', '45W', 'Rugsejis 2013', '3748', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('214', 'AMD A8-5500', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3200 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '3996', '2');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('136', 'AMD FX-4100', 'Zambezi', '4-Core', 'AMD Socket AM3+', '32 nm', '3600 MHz', '192K / 4096K / 8192K', '95W', 'Spalis 2011', '4039', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('107', 'AMD FX-4130', 'Vishera', '4-Core', 'AMD Socket AM3+', '32 nm', '3800 MHz', '192K / 4096K / 8192K', '125W', 'Sausis 2013', '4150', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('202', 'AMD A10-5700', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3400 MHz', '128K / 1024K / 0K', '65W', 'Spalis 2012', '4221', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('209', 'AMD A8-5600K', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3600 MHz', '128K / 1024K / 0K', '100W', 'Spalis 2012', '4330', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('119', 'AMD A8-6500', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '3500 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2013', '4414', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('139', 'AMD FX-4150', 'Zambezi', '4-Core', 'AMD Socket AM3+', '32 nm', '3800 MHz', '192K / 4096K / 8192K', '95W', 'Spalis 2012', '4539', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('118', 'AMD Phenom II X4 975 BE', 'Deneb', '4-Core', 'AMD Socket AM3', '45 nm', '3600 MHz', '128K / 512K / 6144K', '125W', 'Sausis 2011', '4598', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('120', 'AMD Phenom II X4 980 BE', 'Deneb', '4-Core', 'AMD Socket AM3', '45 nm', '3700 MHz', '128K / 512K / 6144K', '125W', 'Geguze 2011', '4609', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('129', 'AMD A10-5800B', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3800 MHz', '128K / 1024K / 0K', '100W', 'Spalis 2012', '4612', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('232', 'AMD A8-6600K', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '3900 MHz', '128K / 1024K / 0K', '100W', 'Birzelis 2013', '4620', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('347', 'AMD A10-6700', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '3700 MHz', '128K / 1024K / 0K', '65W', 'Birzelis 2013', '4624', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('109', 'AMD A10-6790K', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '4000 MHz', '128K / 1024K / 0K', '100W', 'Spalis 2013', '4626', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('114', 'AMD A10-5800K', 'Trinity', '4-Core', 'AMD Socket FM2', '32 nm', '3800 MHz', '128K / 1024K / 0K', '100W', 'Spalis 2012', '4639', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('74', 'AMD FX-4300', 'Vishera', '4-Core', 'AMD Socket AM3+', '32 nm', '3800 MHz', '192K / 4096K / 8192K', '95W', 'Spalis 2012', '4640', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('130', 'AMD FX-4170', 'Zambezi', '4-Core', 'AMD Socket AM3+', '32 nm', '4200 MHz', '192K / 4096K / 8192K', '125W', 'Vasaris 2012', '4821', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('120', 'AMD A10-6800K', 'Richland', '4-Core', 'AMD Socket FM2', '32 nm', '4100 MHz', '128K / 1024K / 0K', '100W', 'Birzelis 2013', '4929', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('92', 'AMD FX-4350', 'Vishera', '4-Core', 'AMD Socket AM3+', '32 nm', '4200 MHz', '288K / 4096K / 8192K', '125W', 'Balandis 2013', '5210', '3');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('100', 'AMD A10-7700K', 'Kaveri', '4-Core', 'AMD Socket FM2', '28 nm', '3500 MHz', '128K / 1024K / 0K', '95W', 'Sausis 2014', '5238', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('199', 'AMD FX-6100', 'Zambezi', '6-Core', 'AMD Socket AM3+', '32 nm', '3300 MHz', '288K / 6144K / 8192K', '95W', 'Spalis 2011', '5405', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('100', 'AMD FX-8140', 'Zambezi', '8-Core', 'AMD Socket AM3+', '32 nm', '3200 MHz', '384K / 8192K / 8192K', '95W', 'Spalis 2012', '5449', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('112', 'AMD A10-7850K', 'Kaveri', '4-Core', 'AMD Socket FM2', '28 nm', '3700 MHz', '128K / 1024K / 0K', '95W', 'Sausis 2014', '5575', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('129', 'AMD FX-8100', 'Zambezi', '8-Core', 'AMD Socket AM3+', '32 nm', '2800 MHz', '384K / 8192K / 8192K', '95W', 'Spalis 2011', '6085', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('113', 'AMD FX-6200', 'Zambezi', '6-Core', 'AMD Socket AM3+', '32 nm', '3800 MHz', '288K / 6144K / 8192K', '125W', 'Vasaris 2012', '6100', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('74', 'AMD FX-6300', 'Vishera', '6-Core', 'AMD Socket AM3+', '32 nm', '3500 MHz', '288K / 6144K / 8192K', '95W', 'Spalis 2012', '6347', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('172', 'AMD FX-8120', 'Zambezi', '8-Core', 'AMD Socket AM3+', '32 nm', '3100 MHz', '384K / 8192K / 8192K', '125W', 'Spalis 2011', '6572', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('104', 'AMD FX-6350', 'Vishera', '6-Core', 'AMD Socket AM3+', '32 nm', '3900 MHz', '288K / 6144K / 8192K', '125W', 'Balandis 2013', '6985', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('136', 'AMD FX-8320E', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '3200 MHz', '384K / 8192K / 8192K', '95W', 'Rugsejis 2014', '7411', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('154', 'AMD FX-8300', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '3300 MHz', '384K / 8192K / 8192K', '95W', 'Spalis 2012', '7599', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('218', 'AMD FX-8150', 'Zambezi', '8-Core', 'AMD Socket AM3+', '32 nm', '3600 MHz', '384K / 8192K / 8192K', '125W', 'Spalis 2011', '7627', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('194', 'AMD FX-8370E', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '3300 MHz', '384K / 8192K / 8192K', '95W', 'Rugsejis 2014', '7826', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('105', 'AMD FX-8320', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '3500 MHz', '384K / 8192K / 8192K', '125W', 'Spalis 2012', '8025', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('138', 'AMD FX-8350', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '4000 MHz', '384K / 8192K / 8192K', '125W', 'Spalis 2012', '8963', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('150', 'AMD FX-8370', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '4000 MHz', '384K / 8192K / 8192K', '125W', 'Rugsejis 2014', '8994', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('160', 'AMD FX-9370', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '4400 MHz', '384K / 8192K / 8192K', '220W', 'Liepa 2013', '9534', '4');
INSERT INTO `cpu`(`price`, `name`, `codename`, `cores`, `socket`, `process`, `clock`, `cache`, `tdp`, `release`, `benchmark`, `class`)
VALUES ('181', 'AMD FX-9590', 'Vishera', '8-Core', 'AMD Socket AM3+', '32 nm', '4700 MHz', '384K / 8192K / 8192K', '220W', 'Liepa 2013', '10266', '4');
